package com.example.travelingtord.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.travelingtord.Fragments.WelcomeFragment
import com.example.travelingtord.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_container, WelcomeFragment())
            .commitAllowingStateLoss()
    }
}