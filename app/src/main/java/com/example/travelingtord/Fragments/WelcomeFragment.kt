package com.example.travelingtord.Fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.travelingtord.R
import com.example.travelingtord.activities.LoginActivity
import com.example.travelingtord.activities.RegisterActivity

class WelcomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_welcome, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val mLogin: Button = view.findViewById(R.id.login)
        val mRegister: Button = view.findViewById(R.id.register)

        mLogin.setOnClickListener {

            val intent = Intent(it.context, LoginActivity::class.java)
            startActivity(intent)
            Toast.makeText(this.context, "login", Toast.LENGTH_SHORT).show()

        }

        mRegister.setOnClickListener {
            val intent = Intent(it.context, RegisterActivity::class.java)
            startActivity(intent)
            Toast.makeText(this.context, "register", Toast.LENGTH_SHORT).show()

        }
    }

}